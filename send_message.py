import inject
from html import unescape
from google.protobuf.json_format import MessageToDict
from google.protobuf.empty_pb2 import Empty as EmptyResponse
from gitlab_grpc_package.chat.send_message import send_message_pb2

from chat.exceptions import MessageOrMediaRequired
from chat.schemas.schemas import DirectMessageCreate
from chat.schemas import requests as request_models, responses as response_models, enums
from chat.use_cases.utils import get_broadcast_message

from repositories import ApiRepo
from pg_repositories.direct_room_repo import SQLAlchemyDirectDBRepository
from chat.use_cases import UseCases
from worker.celery import CeleryRepo


class SendMessageDirect(UseCases):
    validation_model = request_models.SendMessage
    response_model = response_models.SendMessage

    @inject.autoparams('api_repo', 'celery_repo', 'db_repo')
    def __init__(
        self,
        params: send_message_pb2.SendMessageRequest,
        *,
        api_repo: ApiRepo,
        celery_repo: CeleryRepo,
        db_repo: SQLAlchemyDirectDBRepository
    ):
        super().__init__(api_repo, celery_repo, db_repo)
        self.params = self.validate_params(params)

    def validate_params(self, params):
        return self.validation_model(
            **MessageToDict(
                message=params,
                preserving_proto_field_name=True,
                use_integers_for_enums=True,
            )
        )

    def interact(self):
        request_user_id = self.params.request_user_id
        request_id = self.params.request_id
        room_id = self.params.room_id
        message_text = self.params.message
        media_ids = self.params.media_ids
        subject_premises_id = self.params.subject_premises_id

        if not message_text.replace(' ', '') and not media_ids:
            raise MessageOrMediaRequired

        chat_users_ids, _ = self.get_users_data(
            room_id=room_id, request_user_id=request_user_id
        )

        self._db_repo.check_blocked_user(room_id=room_id, user_id=request_user_id)
        current_subject_id = self.check_room_subject(
            room_id=room_id,
            user_id=request_user_id,
            subject_premises_id=subject_premises_id,
            chat_users_ids=chat_users_ids,
        )

        message = DirectMessageCreate(
            payload={'text': unescape(message_text)},
            subject_premises_id=subject_premises_id or current_subject_id,
            sender_id=request_user_id,
            chat_room_id=room_id,
            message_type=enums.MessageType.TEXT,
        )

        message_entity = self._db_repo.create_message(
            message=message,
            room_id=room_id,
            user_id=request_user_id,
            media_ids=media_ids,
        )

        self._db_repo.check_left_users_and_set_false(
            room_id=room_id, users_ids=chat_users_ids
        )
        self._db_repo.last_read_messages.read_messages(
            room_id=room_id,
            user_id=request_user_id,
            last_message_id=message_entity.inner_room_id,
        )

        current_user_info = self._api_repo.auth.get_user_info(
            user_id=request_user_id,
            request_user_id=request_user_id,
            request_id=request_id,
        )
        current_user_info.update(
            {
                'role': self.get_user_role(
                    room_premises_id=None,
                    current_subject_id=current_subject_id,
                    user_id=request_user_id,
                ),
                'last_seen': self.get_user_last_seen(user_id=request_user_id),
            }
        )

        dict_message = message_entity.to_dict()
        dict_message.update(
            {
                'chat_room_id': room_id,
                'is_read': False,
                'sender': current_user_info,
                'payload': {
                    'media': [media.to_dict() for media in message_entity.media]
                },
            }
        )

        message_to_ws = self.response_model(
            **get_broadcast_message(
                payload={'room_id': room_id, 'message': dict_message},
                action_type=enums.ChatActionType.MESSAGE,
            )
        ).dict()

        for user_id in chat_users_ids:
            self.send_message_to_websockets(user_id=user_id, message=message_to_ws)

        self.push_chat_notification_message_to_room(
            request_user_id=request_user_id,
            room_id=room_id,
            chat_users_ids=chat_users_ids,
            message=message_to_ws,
        )

        return EmptyResponse()
