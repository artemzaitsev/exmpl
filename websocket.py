from fastapi import APIRouter, Depends
from starlette.concurrency import run_until_first_complete
from starlette.websockets import WebSocket, WebSocketDisconnect

from src.gateway.connection_managers import websocket_connection_manager as websocket_manager
from src.gateway.repositories.api import ApiRepo
from src.cache.fastapi_cache.fastapi_cache.backends.redis import RedisCacheBackend
from src.redis_cache import users_online_info_cache


router = APIRouter()
api_repo = ApiRepo


async def process_websockets_messages(
        websocket: WebSocket,
        channel,
        request_user_id: str,
        online_info_cache
):
    try:
        while True:
            data = await websocket.receive_json()
            await api_repo.chat.send_by_action(
                websocket=websocket,
                channel=channel,
                data=data,
                request_user_id=request_user_id
            )
    except WebSocketDisconnect:
        await websocket_manager.disconnect(
            user_id=request_user_id,
            websocket=websocket,
            online_info_cache=online_info_cache
        )
        await api_repo.chat.chat_routes.broadcast_online_info_on_disconnect(
            user_id=request_user_id,
            online_info_cache=online_info_cache
        )


async def subscribe_user_topic(websocket, channel):
    async with websocket_manager.broadcaster.subscribe(channel=channel) as subscriber:
        async for event in subscriber:
            await websocket_manager.send_personal_message(
                websocket=websocket,
                response=event.message
            )


@router.websocket('/')
async def chat_websocket(
        websocket: WebSocket,
        online_info_cache: RedisCacheBackend = Depends(users_online_info_cache),
):
    if not (request_user_id := await websocket_manager.check_auth(websocket)):
        return await websocket_manager.auth_failed_error(websocket)

    await websocket_manager.connect(
        websocket=websocket,
        user_id=request_user_id,
        online_info_cache=online_info_cache
    )
    channel = await api_repo.chat.get_chat_channel()

    await api_repo.chat.chat_routes.broadcast_online_info_on_connect(
        user_id=request_user_id,
        online_info_cache=online_info_cache
    )

    process_websockets_messages_data = {
        'websocket': websocket,
        'channel': channel,
        'request_user_id': request_user_id,
        'online_info_cache': online_info_cache,
    }
    subscribe_user_topic_data = {
        'websocket': websocket,
        'channel': request_user_id,
    }
    await run_until_first_complete(
        (process_websockets_messages, process_websockets_messages_data),
        (subscribe_user_topic, subscribe_user_topic_data),
    )
